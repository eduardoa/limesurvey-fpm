#!/bin/bash
set -e

SMTP_HOST=${SMTP_HOST:-'cernmx.cern.ch'}
SMTP_PORT=${SMTP_PORT:-'25'}
SMTP_PROTOCOL=${MAIL_PROTOCOL:-'smtp'}
SMTP_AUTH=${SMTP_AUTH:-'off'}
SMTP_USERNAME=${SMTP_USERNAME:-}
SMTP_PASSWORD=${SMTP_PASSWORD:-}

SMTP_TLS=${SMTP_TLS:-'off'}
SMTP_STARTTLS=${SMTP_STARTTLS:-'off'}
SMTP_TLS_CHECK=${SMTP_TLS_CHECK:-'off'}
SMTP_TLS_TRUST_FILE=${SMTP_TLS_TRUST_FILE:-}

MAIL_FROM_DEFAULT=${MAIL_FROM_DEFAULT:-'noreply@$NAMESPACE.web.cern.ch'}
MAIL_TRUST_DOMAIN=${MAIL_TRUST_DOMAIN:-}
MAIL_DOMAIN=${MAIL_DOMAIN:'cern.ch'}
SMTP_TIMEOUT=${SMTP_TIMEOUT:-'30000'}

PUBLIC_URL=${PUBLIC_URL:-}

DB_HOST=${DB_HOST}
DB_PORT=${DB_PORT}
DB_NAME=${DB_NAME}
DB_USERNAME=${DB_USERNAME}
DB_PASSWORD=${DB_PASSWORD}
DB_TABLE_PREFIX=${DB_TABLE_PREFIX:-'lime_'}

MEMCACHE_WEIGHT=${MEMCACHE_WEIGHT:-'100'}

URL_FORMAT=${URL_FORMAT:-'path'}

# Write MSMTP configuration
cat > /etc/msmtprc << EOL
account default
host ${SMTP_HOST}
port ${SMTP_PORT}
protocol ${SMTP_PROTOCOL}
auth ${SMTP_AUTH}
user ${SMTP_USERNAME}
password ${SMTP_PASSWORD}
tls ${SMTP_TLS}
tls_starttls ${SMTP_STARTTLS}
tls_certcheck ${SMTP_TLS_CHECK}
tls_trust_file
from ${MAIL_FROM_DEFAULT}
maildomain ${MAIL_TRUST_DOMAIN}
domain ${MAIL_DOMAIN}
timeout ${SMTP_TIMEOUT}
EOL

# Write Database config
echo 'Using MySQL'
if ! mysql -h ${DB_HOST} -u $DB_USERNAME -p $DB_PASSWORD $DB_NAME -e "SELECT 1,table_schema,table_name FROM information_schema.tables WHERE table_schema = '${DB_NAME}' AND table_name LIKE '${DB_TABLE_PREFIX}_%';" | grep 1 ; then
    echo 'Database not initialized'
else
    echo 'Database already initialized'

    # If there are files in /tmp/configmap that are not empty
    # (overriden by a ConfigMap) copy them to /limesurvey/application/config
    if [ -n "$(ls -A /tmp/limesurvey-configmap)" ]
    then
        for f in /tmp/limesurvey-configmap/*
        do
            if [ -s $f ]
            then
            cp /tmp/limesurvey-configmap/* ${LIMESURVEY_PATH}/application/config/config.php
            fi
        done        
        # Replace environment variables
        # This is mandatory as at the time of doing the migration, discourse needs to know redis IP,
        # which is set thanks to discourse.conf file.
        echo "--> Overwritting env variables ..."
        envsubst < /tmp/limesurvey-configmap/config.php > ${LIMESURVEY_PATH}/application/config/config.php
        echo "--> DONE"
    fi
fi

# Start Aphache
exec "$@"